#Kata and Tool Sharpening

> “For the things we have to learn before we can do them, we learn by doing them.” -Aristotle

##Kata

Each directory (except `exercism`; that's my exercism solutions) contains a different code-cata, with criteria, limitations, rationale, and some of my attempts to date. Some of them are in progress. They target specific tasks (http-serving, textr transformation, file-system access, etc.) and are intended to expose some sharp edges for your skills to avoid.

Should add a sudoku solver

##Core Compentencies

These are the force multipliers, the things that will be useful in many contexts or let you link together other tools for maximum effectiveness.

* SQL
* Regular Expressions
* Web Servers
* Scripting
* Data Strucutres and Algorithms
* Patterns
* Writing
* Talking


##Target Languages

These are the languages that I either know or aspire to know. They're good candidates for solutions (depending on the problem). There are many others that could make this list; the idea is to have a **small collection** of languages that are **practical for use** and will expose me to a **broad cross-section** of idioms and ideas. This is to minimize the overhead of **focussed, effortful practice**.

 * Python
 * Clojure/ClojureScript
 * Elm
 * Groovy
