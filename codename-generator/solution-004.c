#include<stdio.h>
#include<stdlib.h>
#include<time.h>

// To build: gcc solution-04.c -o pword

#define PWORD_SIZE 16
int rand_inited = 1;

void rand_init() {
  srandom(time(NULL));
  rand_inited = 0;
}

char random_char() {
  if (rand_inited != 0) rand_init();
  char* alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
  int i = random() % 70;
  char c = alphabet[i];
  return c;
}

int main(int argc, char** argv) {
  char pword[PWORD_SIZE+1];
  int i;

  for (i=0; i<PWORD_SIZE; i++) {
    pword[i] = random_char();
  }
  pword[PWORD_SIZE] = 0;
  printf("%s\n", pword);
}
