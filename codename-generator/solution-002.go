package main

// This program pulls down a "semantic encoding word list" when it's
// first run and then serves 

// Copyright 2015 Nathaniel Knight (nathaniel.ep@gmail.com)
// Provided free/libre under the MIT License (http://opensource.org/licenses/MIT)

import (
	"net/http"
	"html/template"
	"math/rand"
	"io/ioutil"
	"strings"
	"fmt"
)

//Fetch a list of memorable words and put them in an array.
func getWordList() []string {
	wordlist_url := "https://gist.githubusercontent.com/ehedaya/2743dce1f0d1e8375367/raw/11aab64744b9dd09a105ab8e406244c530d4c856/wordlist.txt"
	resp, err := http.Get(wordlist_url)
	if err != nil {
		panic("Couldn't get the wordlist!")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	//Show
	var wordlist []string
	for i, line := range strings.Split(string(body), "\n") {
		if i != 0 { //skip title line
			for _, word := range strings.Split(line, " ") {
				word = strings.TrimSpace(word)
				if word != "" {
					wordlist = append(wordlist, word)
				}
			}
		}
	}
	return wordlist
}


//Template for Presentation (uses Google Fonts)
var codenameTemplate = template.Must(template.New("codename").Parse(`
<html> <head> <title>Codename</title> <link
href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet'
type='text/css'> <style> body { width: 16em; text-align: center;
font-family: 'Lato', sans-serif; margin-left: auto; margin-right:
auto; margin-top: 2em; } p { font-size: 14pt; } </style> </head>
<body> <h1>Codename</h1> <p> {{index .Words 0}} {{index .Words 1}}
{{index .Words 2}} {{index .Words 3}} </p> <address> <a
href="mailto:nathaniel.ep@gmail.com"></a> </address> </body>
</html>`))

type wordDeck struct {Words []string} // the word list
type wordSet struct {Words [4]string} // the words for a single codename

func (w wordDeck) Pick() wordSet {
	var x wordSet
	for i:=0; i<4; i++ {
		j := rand.Intn(len(w.Words))
		x.Words[i] = w.Words[j]
	}
	return x
}

//Closure for responding to web requests
func makeCodenameHandler(d wordDeck) func (w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request){
		s := d.Pick()
		codenameTemplate.ExecuteTemplate(w, "codename", s)
	}
}

//------------------------------------------------------------------

func main () {
	deck := wordDeck{Words: getWordList()}
	codenameHandler := makeCodenameHandler(deck)

	http.HandleFunc("/", codenameHandler)
	http.ListenAndServe("localhost:5622", nil)

	fmt.Println("Your Codename server is running!")
	fmt.Println("Point your browser at 'http://localhost:5562' to get your codename.")

}
//TODO: could try to open a browser in a goroutine based on OS
