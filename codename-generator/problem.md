# Codename Generator

Write a program to generate a probably-unique codename.

This could be a "word word" style codename ("jumping zebra", "eloquent
jest") or a simple unique string ("as929cb9s0s") suitable for use as a
password. 

Your program could be a command line program, a GUI, a web-page +
server, whatever you like. The point is to come up with a good scheme
for generating a random name.
