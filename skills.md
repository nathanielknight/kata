#Core Skills / To-Learn List

 * Tools
   * git (hooks, rebase)
   * Docker

 * Protocols
   * DNS
   * TCP/IP
   * HTTP
   * SMTP
   * SSH

 * Fundamental Algorithm/Problem pairs
   * Hashing
   * Encryption & Cryptography
   * Random number generation

 * Fundamental Tools
   * SQL
   * Regular Expressions

 * Algorithms, Data Structures, and Patterns
   * Hash-Map
   * Sorting Algorithms
   
 * Language Categories and Paradigms
   * Lisp/Clojure
   * ML/Elm/Haskell
   * C/Objective-C
   * JS/Purescript/Coffeescript
   * Go
