#Reading

These are things that should be read every so often.

 * [Jeff Atwood's Pragmatic Programmer Cheat sheet](http://blog.codinghorror.com/a-pragmatic-quick-reference)
 * [Learnable Programming](http://worrydream.com/LearnableProgramming/)
 * [The 12 Factor App](http://12factor.net/)
 * [How to Ask Questions the Smart Way](http://www.catb.org/esr/faqs/smart-questions.html)

These are things that should be read at least once.

 * [The Best Regex Trick Ever](http://www.rexegg.com/regex-best-trick.html)
