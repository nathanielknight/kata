#!/usr/bin/env ruby
# Copyright 2014 Nat Egan-Pimblett
# nathaniel.ep@gmail.com
# Sept 17th, 2014
require 'sinatra'
require 'kramdown'


# Args=======================================
mdfilename = ARGV.first

# Compiler===================================
def compile(fname)
  mdDoc = File.open(fname, 'r') do |mdfile|
    mdsource = mdfile.read
    mdsource = %{<style>
body {max-width: 50em;
padding: 2em;
margin-left: auto;
margin-right: auto;}
</style>
} + mdsource
    Kramdown::Document.new(mdsource)
  end
  mdDoc.to_html
end

# Open Browser===============================
IO.popen("firefox 'http://localhost:4567'")

# Server=====================================
# class MDServer < Sinatra::Base
#   get '/' do
#     compile(mdfilename)
#   end

#   run! if app_file = $0
# end
get '/' do
  compile(mdfilename)
end

get '/style.css' do
  "body {max-width: 50em; margin-left: auto; margin-right: auto; padding: 2em;}"
end
