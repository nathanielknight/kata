#Markdown Server
##The Problem
Write a program that, when called from a directory containing Markdown files, parses and serves them on a port on the local machine.

##Bonus Features
 * Serve JS, CSS, and media files.
 * Accept command line arguments for serving port, target directory, recursion.
 * What do you do about an index file?
 * Offer admin tools (e.g. show when a file's been changed, what all the files are).
 * Package for distribution (gem, pip, lein, etc.).


##The Constraints
This shouldn't be an exercise in using a static site generator; it also shouldn't be an exercise in writing a Markdown parser! Using a Markdown library is fine, and using standard http-server tools (e.g. the Python Standard Library, Ring, Sinatra, etc.) is expected, but don't just use a tool like Nanoc. This is a kata: the gritty details and edge cases shouldn't be taken care of for you, even if a tool exists to do it well.
Other things that could vary are

 * Object Oriented vs. Functional vs. Declarative vs. Imperative style
 * Single-file vs. Library architecture
 * Time constraints
 * . . . what else?

##Target Languages
 * [ ] Clojure
 * [ ] Python
 * [ ] Ruby
 * [ ] C
 * [ ] D
 * [ ] Shell
 * [ ] Haskell
 * [ ] Go