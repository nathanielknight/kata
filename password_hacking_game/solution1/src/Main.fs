(* A mastermind-style console game. The user is presented with a list of
words (all of the same clength) and makes guesses as to which one is
"the password". They're told how many letters their guess shares with the 
password and allowed to guess again (up to a total of six times). *)

module Main

type Game = {
    password:string;
    candidates:List<string>;
    guesses:List<string>;
}


// Settings
let candidateCount = 6
let maxGuesses = 4
let passwordList = [ "TOENAIL"; "ELATION"; "ROUTINE"; "ATONIES";
"OUTEARN"; "URINATE"; "URANITE"; "TAURINE"; "RUINATE"; "ALIENOR";
"AILERON"; "ERASION"; "TRENAIL"; "RETINAL"; "RELIANT"; "RATLINE";
"LATRINE"; "ANEROID"; "TRAINEE"; "RETINAE"; "ARENITE"; "INERTIA";
"AEOLIAN"; "TRAINED"; "DETRAIN"; "ANTIRED"; "NIOBATE"; "ACONITE";
"RONDEAU"; "RAINOUT"; "NEUROID"; "DOURINE"; "URANIDE"; "UNAIRED";
"STONIER"; "ORIENTS"; "OESTRIN"; "NORITES"; "ENATION"; "ALEURON";
"STEARIN"; "STAINER"; "RETSINA"; "RETINAS"; "RETAINS"; "RATINES";
"NASTIER"; "ANTSIER"; "ANESTRI"; "ALUNITE"; "ALIENER"; "TREASON";
"SENATOR"; "ATONERS"; "OUTLIER"; "ROMAINE"; "NEUTRAL"; "MORAINE";
"AIRLINE"; "REGINAE"; "NITERIE"; "UTERINE"; "REUNITE"; "RETINUE";
"OUTLINE"; "ELUTION"; "DENARII"; "TORULAE"; "INEDITA"; "RETINOL";
"DIATRON"; "TEARING"; "TANGIER"; "REPAINT"; "PERTAIN"; "PAINTER";
"LINEATE"; "INGRATE"; "GRATINE"; "GRANITE"; "AMNIOTE"; "RATIONS";
"FOLIATE"; "AROINTS"; "ARENOUS"; "URINOSE"; "TRAILED"; "REDTAIL";
"ETESIAN"; "DILATER"; "URALITE"; "SOUTANE"; "DARIOLE"; "AUDIENT";
"OUTLAIN"; "EROTICA"; "ENTRAIN"; "VIOLATE"; "UNITIES"; "ENACTOR"; ]


// Utilities

let commonLetters (x:string) (y:string) = 
    let pairs = Seq.zip (x.ToUpper()) (y.ToUpper())
    Seq.length (Seq.filter (fun (a,b) -> a.Equals(b)) pairs)

let isOver (g:Game) =
    (g.guesses.Length) >= maxGuesses

let passwordGuessed (g:Game) =
    List.contains g.password g.guesses

let generateGame =
    let mutable rand = System.Random()
    let randomPassword:string =
        let idx = rand.Next(0, passwordList.Length)
        List.item idx passwordList
    let rec passwords ps =
        if (Set.count ps) > candidateCount then
            ps
        else
            printfn "%s" (ps.ToString())
            passwords (Set.add (randomPassword) ps)
    let actualPassword = (randomPassword)
    { password = actualPassword;
      candidates = (Set [actualPassword] |> passwords) |> Set.toList ;
      guesses = [];
    }


// Operations

let applyGuess (guess:string) (game:Game) =
    let newGuesses = List.append game.guesses [guess]
    { game with guesses = newGuesses }


// Views

let renderSetup (game:Game) =
    let candidates' = List.map (fun (s:string) -> s.ToUpper()) game.candidates
    sprintf "Scanning memory...done\nPassword Candidates:\n%s\nBeginning Hack..." (String.concat "\n" candidates')

let renderGuesses (game:Game) =
    let renderGuess (g:string) =
        sprintf "Tried: %s\n%d characters in common\n" (g.ToUpper()) (commonLetters g game.password)
    String.concat "\n" (List.map renderGuess game.guesses)

let renderInterface (game:Game) =
    if (passwordGuessed game) then
        "Hack success!\n"
    elif (isOver game) then  
        "Hack failed.\n"
    else 
        "> "

let renderGame (game:Game) =
    String.concat "\n" [
        renderSetup game;
        renderGuesses game;
        renderInterface game;
    ]


// Main

let testGame:Game = {
    candidates = ["a"; "b"; "c"; "d"];
    password = "D";
    guesses = [];
}

let rec play (g:Game) =
    System.Console.Clear()
    renderGame g |> System.Console.Write
    if (isOver g) || (passwordGuessed g) then
        ()
    else
        let guess = (System.Console.ReadLine()).ToUpperInvariant()
        let g' = applyGuess guess g
        play g'


[<EntryPoint>]
let main argv =
    ignore (play testGame)
    0
