(define (inc n) (+ n 1))

(define (next-pascal-row xs)
  (map + (append xs '(0)) (append '(0) xs)))

(define (pasc-rows n)
  (define (pr rows i)
    (if (= i n)
        (reverse rows)
        (pr
         (cons
          (next-pascal-row (car rows))
          rows)
         (inc i))))
  (pr '((1)) 0))


(define (print-pasc-row row)
  (map (lambda (x) (printf "~a " x)) row)
  (printf "~n"))

(define (pasc n)
  (map print-pasc-row (pasc-rows n)))

(pasc 20)
