# Pascal's Triangle

Write a program that prints the first 13 lines
of [Pascal's Triange][0].

[0]: https://en.wikipedia.org/wiki/Pascal%27s_triangle

Assume that whoever is executing the program can figure out how
(e.g. will find your machine's Ruby installation). The output should
look like this:

```
1 
1 1 
1 2 1 
1 3 3 1 
1 4 6 4 1 
1 5 10 10 5 1 
1 6 15 20 15 6 1 
1 7 21 35 35 21 7 1 
1 8 28 56 70 56 28 8 1 
1 9 36 84 126 126 84 36 9 1 
1 10 45 120 210 252 210 120 45 10 1 
1 11 55 165 330 462 462 330 165 55 11 1 
1 12 66 220 495 792 924 792 495 220 66 12 1 
1 13 78 286 715 1287 1716 1716 1287 715 286 78 13 1 
```
