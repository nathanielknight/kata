fn factorial(n: i32) -> i32 {
    if n <= 1 {
        1
    } else {
        n * factorial(n - 1)
    }
}


fn ncr(n: i32, r: i32) -> i32 {
    factorial(n) / (factorial(r) * factorial(n - r))
}


fn pasc_row(i: i32) {
    for j in 0..i {
        print!("{} ", ncr(i,j));
    }
    print!("\n");
}


fn pasc(n: i32) {
    for i in 0..n {
        pasc_row(i);
    }
}

fn main () {
    pasc(13);
}


