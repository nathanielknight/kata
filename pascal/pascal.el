(defun factorial (n)
  (if (< n 2)
      1
    (* n (factorial (- n 1)))))

(defun ncr (n r)
  (/ (factorial n)
     (* (factorial r) (factorial (- n r)))))

(defun pasc-row (i)
  (mapcar (lambda (j) (ncr i j))
          (number-sequence 0 i)))

(defun pasc-row-str (i)
  (concat
   (mapconcat (lambda (n) (format "%d "n))
              (pasc-row i)
              " ")     
   "\n"))

(defun pasc (n)
  (mapcar (lambda (i) (princ (pasc-row-str i)))
          (number-sequence 1 n)))


(pasc 13)
