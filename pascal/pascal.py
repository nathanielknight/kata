def factorial(n):
    if n <= 1:
        return 1
    else:
        return n * factorial(n-1)


def ncr(n,r):
    return factorial(n) / (factorial(r) * factorial(n-r))


def pasc_row(i):
    print(" ".join(
        map(
            lambda j: str(ncr(i,j)),
            range(i))))

def pasc(n):
    for i in range(n):
        pasc_row(i)


if __name__ == "__main__":
    pasc(13)
