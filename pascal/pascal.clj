(defn fac [n]
  (if (<= n 1)
    1
    (* n (-> n dec fac))))

(defn ncr [n r]
  (/ (fac n)
     (* (fac r) (fac (- n r)))))


(defn pasc-row [n]
  (map (partial ncr n) (range (inc  n))))


(defn pasc [n]
  (doseq [i (range n)]
         (println (pasc-row i))))

(pasc 13)
