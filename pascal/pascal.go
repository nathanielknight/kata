package main

import ("fmt")


func factorial(n int) int {
	if (n <= 1) {
		return 1
	} else {
		return n * factorial(n-1)
	}
}

func ncr(n,r int) int {
	return factorial(n) / (factorial(r) * factorial(n-r))
}


func pasc_row(i int) {
	for j:=0; j<=i; j++ {
		fmt.Print(ncr(i,j), " ")
	}
	fmt.Print("\n")
}


func main () {
	for n:=0; n<13; n++ {
		pasc_row(n)
	}
}

