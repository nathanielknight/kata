-module(asdf).

factorial(N) when N < 2 ->
    1;
factorial(N) -> 
    N * factorial(N - 1).

ncr(N,R) ->
    round(factorial(N) / (factorial(R) * factorial(N - R))).

pasc_line(I) ->
    lists:map(fun(J) -> ncr(I,J) end,
              lists:seq(0,I)).

print_ints(XS) ->
    lists:map(fun(N) -> io:format("~B ", [N]) end,
              XS).

print_pasc_line(I) ->
    print_ints(pasc_line(I)),
    io:format("~n").


main(_) ->
    lists:map(fun(I) -> print_pasc_line(I) end,
              lists:seq(0,13)).

