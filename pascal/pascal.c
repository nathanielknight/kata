#include<stdio.h>

#define N 13

int factorial(int n) {
  if (n <= 1) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}


int ncr(int n, int r) {
  return factorial(n) / (factorial(r) * factorial(n - r));
}

void pasc_row(int i) {
  int j, n;
  for (j=0; j<=i; j++) {
    n = ncr(i,j);
    printf("%d ", n);
  }
  printf("\n");
}

/* Print n rows of pascal's triangle */
void pasc(int n) {
  int i;
  for (i=0; i<=N; i++) {
    printf("%d : ", i);
    pasc_row(i);
  }
}


int main (int argc,  char** argv) {
  pasc(N);
}
  
