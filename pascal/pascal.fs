: dec 1 - ;
: inc 1 + ;

: factorial ( n )
  1 swap
  inc
  1 ?do
    i *
  loop
;

: ncr ( n r )
  over over
  -
  factorial
  rot
  factorial
  rot
  factorial
  rot
  *
  /
;

variable rowsize

: pasc_row ( n )
  rowsize !
  rowsize @ inc
  0 do
    rowsize @
    i
    ncr .
  loop
  cr
;

: pasc ( n )
  0 ?do
    i pasc_row
  loop
;

13 pasc

bye
