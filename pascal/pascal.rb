def factorial(n)
  if n <= 1
    return 1
  else
    return n * factorial(n-1)
  end
end


def ncr(n,r)
  factorial(n) / (factorial(r) * factorial(n-r))
end


def pasc_row(i)
  xs = (0..i).map {|j| ncr(i,j)}
  xs.map {|x| x.to_s} .join(" ")
end


def pasc(n)
  for i in 0..n
    puts pasc_row(i)
  end
end

pasc 13

