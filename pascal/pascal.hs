initPascLine = [1]

nextPascLine xs = zipWith (+) ([0]++xs) (xs++[0])

pascLines = [1] : [nextPascLine x | x <- pascLines]

pascLineRepr xs =
  let
    c = show (head xs)
    cs = map show (tail xs)
  in
    foldl (\xs x -> xs ++ " " ++ x) c cs


printPascLine xs = putStrLn (pascLineRepr xs)

printPascLines n = mapM printPascLine (take n pascLines)

main = do (printPascLines 20)
